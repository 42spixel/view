# **Viewer**

Node.js viewer for Docker Swarm.

### **Credits**

+ [dockersamples/docker-swarm-visualizer](https://github.com/dockersamples/docker-swarm-visualizer)

### **MIT License**

This work is licensed under the terms of **[MIT License](https://bitbucket.org/42spixel/view/src/master/LICENSE.md)**.
